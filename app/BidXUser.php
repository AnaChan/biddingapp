<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Exception;
use Carbon\Carbon;

class BidXUser extends Model
{
    protected $fillable = [
        'user_id', 'product_id', 'bid_amount', 'autobidding_enable','updated_at'
    ];

    /**
     * create bid on product
     * @return json
     * @author Ana Rodriguez <anaro87@gmail.com>
     * @param int $idUser
     * @param int $idProduct
     * @param float $bidAmount
     */
    public function createBidByUser($userId, $productId, $bidAmount)
    {
        $res = null;
        // return $this->existBidXUser($userId, $productId)->isEmpty();
        try {
            if($this->existBidXUser($userId, $productId)->isEmpty()){
               // insert
               $res = $this->insertBidByUser($userId, $productId, $bidAmount, null);
            }else{
                // update
                $res = $this->updateBidByUser($userId, $productId, $bidAmount);
            }

            if($res !== false){
                $this->runAutoBidding($productId,$userId);
            }

            return $res;

        } catch(exception $e){
            print $e->getMessage();
            return $res;
        }
    }

    /**
     * get the max bid by product
     * @return float
     * @author Ana Rodriguez <anaro87@gmail.com>
     * @param int $idProduct
     */
    public function getMaxBid($productId)
    {
        $product = New Product();
        $bidAmount = DB::table('bid_x_users')
                ->select('id')
                ->where([ ['product_id', '=', $productId]])
                ->max('bid_amount');
        $maxAmount = $bidAmount === null?$product->getProductinitBid($productId):$bidAmount;

        return $maxAmount;
    }

    /**
     * enable autobbinding
     * @author Ana Rodriguez <anaro87@gmail.com>
     * @param int $idUser
     * @param int $idProduct
     * @param bool $autoBiddingEnable
     */
    public function enableAutobiding($userId, $productId, $autoBiddingEnable)
    {
        if($this->existBidXUser($userId, $productId)->isEmpty()){
            //insert
            $result=$this->insertBidByUser($userId, $productId, null, $autoBiddingEnable);
        } else{
            $result=DB::table('bid_x_users')
            ->where([ ['product_id', '=', $productId],['user_id', '=', $userId]])
            ->update(['autobidding_enable' => $autoBiddingEnable,
            'autobidding_enable_at'=>Carbon::now(),
            'updated_at'=>Carbon::now()]);
        }

        return $result;
        
    }

    //helpers

     /**
     * insert bid on product
     * @return bool
     * @author Ana Rodriguez <anaro87@gmail.com>
     * @param int $idUser
     * @param int $idProduct
     * @param float $bidAmount
     */
    private function insertBidByUser($userId, $productId, $bidAmount, $enableAutobid)
    {
        $cols = ['user_id' => $userId, 'product_id' => $productId, 'bid_amount' => $bidAmount,
        'autobidding_enable'=> $enableAutobid,'created_at'=>Carbon::now()];

        if($enableAutobid !== null){
            $cols['autobidding_enable_at'] = Carbon::now();
        }

        $insert=  DB::table('bid_x_users')->insert($cols);

        return $insert;
    }

    /**
     * update bid on product
     * @return bool
     * @author Ana Rodriguez <anaro87@gmail.com>
     * @param int $idUser
     * @param int $idProduct
     * @param float $bidAmount
     */
    private function updateBidByUser($userId, $productId, $bidAmount)
    {
        return DB::table('bid_x_users')
        ->where([ ['product_id', '=', $productId],['user_id', '=', $userId]])
        ->update(['bid_amount' => $bidAmount, 'updated_at'=>Carbon::now()]);
    }

    /**
     * validate if there is a bid for a product by user
     * @return json
     * @author Ana Rodriguez <anaro87@gmail.com>
     * @param int $idProduct
     * @param int $idUser
     */
    public function existBidXUser($userId, $productId){
        $bidXuser = DB::table('bid_x_users')
                ->select('id')
                ->where([ ['product_id', '=', $productId],['user_id', '=', $userId]])
                ->get();
        
        return $bidXuser;

    }

    /**
     * get bid amount by user id and product id 
     * @return json
     * @author Ana Rodriguez <anaro87@gmail.com>
     * @param int $idProduct
     * @param int $idUser
     */
    public function getbidAmount($userId, $productId){
        $bidXuser = DB::table('bid_x_users')
                ->select('id')
                ->where([ ['product_id', '=', $productId],['id_user', '=', $userId]])
                ->get()->first();
        $amount = $bidXuser->bid_amount === null? 0:$bidXuser->bid_amount;
        
        return $amount;
    }

    /**
     * execute auto bidding functionality
     * @return json
     * @author Ana Rodriguez <anaro87@gmail.com>
     * @param int $idProduct
     * @param int $idUser
     */
    public function runAutoBidding($productId, $userId){
        $user = New User();
        
        try{
            $queryResult = DB::table('bid_x_users')
            ->select('user_id', 'product_id', 'autobidding_enable')
            ->where([ ['product_id','=', $productId], ['autobidding_enable','=',true]])
            ->orderBy('autobidding_enable_at', 'desc')
            ->get();
    
            foreach ($queryResult as $row) {
                $maxBid = $this->getMaxBid($row->product_id);
                $maxAmountAllow = $user->getMaxAmountAllowAutobidById($row->user_id);
                if(($maxBid <= $maxAmountAllow || $maxAmountAllow == null) && $row->user_id !== $userId){
                    DB::table('bid_x_users')
                    ->where([ ['product_id', '=', $productId],['user_id', '=', $row->user_id]])
                    ->update(['bid_amount' => $maxBid + 1]);
                }
            }
        }catch(Exception $e){
            print $e->getMessage();
        }

        
    
        return true; //TODO: add valid json on controller

    }
}

    
