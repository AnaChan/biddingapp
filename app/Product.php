<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Product extends Model
{
    protected $fillable = [
        'name','description','img','init_bid_amount','end_bid_date'
    ];

    /**
     * get all products
     * @return json
     * @author Ana Rodriguez <anaro87@gmail.com>
     * @param string $searchTerm
     */
    public function getAllProducts($searchTerm)
    {
        $products = DB::table('products')
        ->select(['id','name','description', 'img', 'init_bid_amount', 'end_bid_date']);
        
        if($searchTerm !== null){
            $products = $products->where([['name', 'LIKE', '%'. $searchTerm .'%']]);
            $products = $products->orWhere([['description', 'LIKE', '%'. $searchTerm .'%']]);
        }
        $products = $products->paginate(10);
        return $products;
        
    }

    /**
     * get single products
     * @return json
     * @author Ana Rodriguez <anaro87@gmail.com>
     * @param int $id
     */
    public function getSingleProducts($id)
    {
        $bidXUser = New BidXUser();
        $maxBid = $bidXUser->getMaxBid($id);
        $product = DB::table('products')
        ->select(['id','name','description', 'img', 'init_bid_amount', 'end_bid_date'])
        ->where([ ['id', '=', $id]])
        ->get()->first();
        $result =  array('id'=>$product->id,
                        'name'=>$product->name,
                        'description'=>$product->description, 
                        'img'=>$product->img, 
                        'init_bid_amount'=>$product->init_bid_amount, 
                        'end_bid_date'=>$product->end_bid_date,
                        'max_bid'=>$maxBid);
        return $result;
    }

    /**
     * get product init bid
     * @return json
     * @author Ana Rodriguez <anaro87@gmail.com>
     * @param int $id
     */
    public function getProductinitBid($id){
        $product = DB::table('products')
                ->select('init_bid_amount')
                ->where([ ['id', '=', $id]])
                ->get()->first();
        $initAmount = $product->init_bid_amount !== null? $product->init_bid_amount:0;
        return $initAmount;
    }

    /**
     * set bid enDate
     * @return json
     * @author Ana Rodriguez <anaro87@gmail.com>
     * @param int $id
     * @param string $endDate
     */
    public function setBidEndDateTime($id, $endDate){
        $product = DB::table('products')
        ->where([ ['id', '=', $id] ])
        ->update(['end_bid_date' => $endDate,
        'updated_at'=>Carbon::now()]);
        
        return $product;
    }

    
}
