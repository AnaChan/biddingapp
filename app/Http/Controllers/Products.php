<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Product;

class Products extends Controller
{
    /**
     * get all products
     * @return json
     * @author Ana Rodriguez <anaro87@gmail.com>
     */
    public function getProducts(Request $request){
        $product = New Product();
        return $product->getAllProducts($request->searchTerm);
    }

    /**
     * get single products
     * @return json
     * @author Ana Rodriguez <anaro87@gmail.com>
     * @param int $id
     */
    public function getProduct($id){
        $product = New Product();
        return $product->getSingleProducts($id);
    }

    /**
     * set end date time
     * @return json
     * @author Ana Rodriguez <anaro87@gmail.com>
     * @param int $id
     */
    public function setProductEndBidDateTime(Request $request){
        $product = New Product();
        $result = $product->setBidEndDateTime($request->id, $request->endDate);
        return array("result" => $result);
    }

}
