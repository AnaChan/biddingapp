<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * login
     * @return json
     * @author Ana Rodriguez <anaro87@gmail.com>
     */
    public function login(Request $request)
    {
       $user = New User();
       $isValidUser = $user->isValidUser($request->email, $request->pws);
       if ($isValidUser){
           $token = $user->generateToken($request->email);
           $result = array(
            "token" => $token,
            "email" => $request->email,
            "id" => $user->getIdUser($request->email)
            );
           
       }else{$result = array( "error" => '400');
        }

        return $result;
    }
}
