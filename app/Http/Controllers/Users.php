<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class Users extends Controller
{
    /**
     * set max amount for autibidding
     * @return json
     * @author Ana Rodriguez <anaro87@gmail.com>
     * @param int $id
     * @param float $maxAmount
     */
    public function setMaxAmount(Request $request){
        $user = New User();
        $result =  $user->setMaxAmountAutoBidding($request->email, $request->maxAmount);
        return array("result" => $result);
    }

    /**
     * get max amount for autibidding
     * @return json
     * @author Ana Rodriguez <anaro87@gmail.com>
     * @param int $id
     * @param float $maxAmount
     */
    public function getMaxAmount(Request $request){
        $user = New User();
        $maxAmount = $user->getMaxAmountAllowAutobid($request->email);
        return json_encode($maxAmount);
    }

}
