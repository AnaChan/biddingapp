<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;


class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone','max_amount_autobidding', 'updated_at','api_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     * @var array
     */
    protected $casts = [
        'updated_at' => 'datetime',
    ];

    /**
     * validate user
     * @return json
     * @author Ana Rodriguez <anaro87@gmail.com>
     */
    public function isValidUser($email, $pws): bool {
        
        $pwsHash = DB::table('users')->select(['password'])->where([ 
            ['email', '=', $email],
            ])->get()->first()->password;

        $isValid = password_verify($pws, $pwsHash);

        return $isValid;
        
    }

    /**
     * generate access token
     * @return json
     * @author Ana Rodriguez <anaro87@gmail.com>
     * @param string $email
     */
    public function generateToken($email)
    {
        DB::table('users')
            ->where('email', $email)
            ->update(['api_token' => Str::random(60), 'updated_at'=>Carbon::now()]);
        $user = DB::table('users')->select(['api_token'])->where([ 
                ['email', '=', $email],
                ])->get();
        
        return $user->first()->api_token;
    }

    /**
     * set max amount for auto bidding
     * @return json
     * @author Ana Rodriguez <anaro87@gmail.com>
     * @param string $email
     * @param float $maxAmount
     */
    public function setMaxAmountAutobidding($email, $maxAmount)
    {
        $res = DB::table('users')
            ->where('email', $email)
            ->update(['max_amount_allow_autobidding' =>$maxAmount, 'updated_at'=>Carbon::now()]);
        
        return $res;
    }


    /**
     * get max amount allow for auto bidding by user email
     * @return json
     * @author Ana Rodriguez <anaro87@gmail.com>
     */
    public function getMaxAmountAllowAutobid($email)
    {

        $maxAmount = DB::table('users')
        ->select('max_amount_allow_autobidding')
        ->where([['email', '=', $email]])
        ->get()->first();

        $maxAmount = $maxAmount === null? 0:$maxAmount;
        
        return $maxAmount;
    }

    /**
     * get max amount allow for auto bidding by user id
     * @return json
     * @author Ana Rodriguez <anaro87@gmail.com>
     * @param int $id
     */
    public function getMaxAmountAllowAutobidById($id)
    {

        $maxAmount = DB::table('users')
        ->select('max_amount_allow_autobidding')
        ->where([['id', '=', $id]])
        ->get()->first();

        $maxAmount = $maxAmount->max_amount_allow_autobidding === null? 0:$maxAmount->max_amount_allow_autobidding;
        
        return $maxAmount;
    }

    /**
     * get id user
     * @return json
     * @author Ana Rodriguez <anaro87@gmail.com>
     * @param int $id
     * @param float $maxAmount
     */
    public function getIdUser($email){
        $result = DB::table('users')
        ->select('id')
        ->where([['email', '=', $email]])
        ->get()->first();
        
        return $result->id;
    }

}
