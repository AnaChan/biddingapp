#SETUP

-   this project was created using xampp-windows-x64-7.2.31-0-VC15-installer and laravel 5.8
-   clone the project
-   run xampp control
-   Start MySql
-   create database with name bidding_app
-   config database connection on config/database.php
    -   under mysql connection add: DB_HOST, DB_DATABASE, DB_USERNAME, DB_PASSWORD
-   change database name in .env DB_DATABASE
-   (gitbash) go to the project's folder
-   run this command:
    `composer install`
- test users
 - email: lolo@gmail.com pws: VENiG
 - email: temari@gmail.com pws: m9Y2W
 - any user created in the db by the seeder is valid too

##Database creation

-   (gitbash) go to the project's folder

#RUN

-   run server (gitbash) go to the project's folder run:
    `php artisan serve`


#RUN COMMANDS
#run migration

-   create table
    `php artisan migrate`


#run seeds

-   generate the seeds
    `composer dump-autoload`
    `php artisan db:seed`

#run test
`./vendor/bin/phpunit`


#HELP COMMANDS TO CREATE (FOR DEVELOPMENT)
#run reset

-   reset database in case you need to drop all tables
    `php artisan migrate:reset`

#create migreation for table
`php artisan make:migration create_bid_x_user_table --create=bid_x_user`

#create seeder
`php artisan make:seeder UsersTableSeeder`

#Create model
`php artisan make:model Product`

#clean cache
`php artisan cache:clear`
`php artisan config:cache`
`php artisan config:clear`

#create controller
`php artisan make:controller ShowProfile`

#create test case
`php artisan make:test BasicTest`

#about php unit
https://semaphoreci.com/community/tutorials/getting-started-with-phpunit-in-laravel


