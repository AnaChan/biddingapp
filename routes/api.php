<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
});

Route::post('login', 'Auth\LoginController@login');

Route::group(['middleware' => 'auth:api'], function() {
    Route::get('products', 'Products@getProducts');
    Route::get('products/{id}', 'Products@getProduct');
    Route::post('bidXUsers', 'BidXUsers@createBid');
    Route::get('bidXUsers', 'BidXUsers@getMaxBid');
    Route::patch('bidXUsers', 'BidXUsers@enableAutoBidding');
    Route::get('bidXUsers/runautobid', 'BidXUsers@executeAutobidding');//TODO:remove
    Route::patch('users', 'Users@setMaxAmount');
    Route::get('users/maxamount', 'Users@getMaxAmount');
});






