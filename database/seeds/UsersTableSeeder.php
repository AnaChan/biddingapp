<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = factory(User::class)->make();   
        factory(User::class, 5)->create();

        DB::table('users')->insert([
            'name' => 'lolo alberto',
            'password' => password_hash('VENiG', PASSWORD_DEFAULT),
            'api_token' => 'AaAYvoCucLA7jXsmyufK4IC3t3MMo75uzMC5VWK2qyjPb148wjDX4WoZfNsS',
            'email' => 'lolo@gmail.com',
            'remember_token' => '9FC2llfyds',
            'created_at' => Carbon::now(),
        ]);

        DB::table('users')->insert([
            'name' => 'temari burgos',
            'password' => password_hash('m9Y2W', PASSWORD_DEFAULT),
            'api_token' => 'A22AzBZR5jgI9rDyaoBoqOT4VRBy4kcg1pzrcPwBoV1s9z4nUdE6du6XXbaI',
            'email' => 'temari@gmail.com',
            'remember_token' => 'Hs7pgRDijs',
            'created_at' => Carbon::now(),
        ]);
    }
}
