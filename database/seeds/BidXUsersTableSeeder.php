<?php

use Illuminate\Database\Seeder;
use App\BidXUser;


class BidXUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bidXUsers = factory(BidXUser::class)->make();   
        factory(BidXUser::class, 6)->create();
    }
}
