<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Product;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->text(10),
        'description' => $faker->text(300),
        'img' => "https://icon-library.com/images/image-icon/image-icon-16.jpg",
        'init_bid_amount' => mt_rand( 5 , 30 ),
        'end_bid_date' => '2022-06-09 17:18:15',
    ];
});
