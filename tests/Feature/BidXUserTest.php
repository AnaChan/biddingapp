<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\BidXUser;

class BidXUserTest extends TestCase
{
    /**
     * set bid end date.
     * @test
     * @return void
     */
    public function shouldCreateBid()
    {
        $bidXuser = New BidXUser();
        $result = $bidXuser->createBidByUser(1,1,5);
        $result = $result === 1? true:false;
        $this->assertTrue($result);
    }

     /**
     * set bid end date.
     * @test
     * @return void
     */
    public function shouldGetMaxBid()
    {
        $bidXuser = New BidXUser();
        $result = $bidXuser->getMaxBid(1);
        $result = $result > 0? true:false;
        $this->assertTrue($result);
    }

    /**
     * set bid end date.
     * @test
     * @return void
     */
    public function shouldEnableAutoBidding()
    {
        $bidXuser = New BidXUser();
        $result = $bidXuser->enableAutobiding(1, 1, true);
        $result = $result > 0 || $result === true? true:false;
        $this->assertTrue($result);
    }

    //TODO: ANA test
}
