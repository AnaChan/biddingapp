<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Product;
use App\User;
use Illuminate\Support\Facades\DB;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

class ProductTest extends TestCase
{
    /**
     * get product data
     * @test
     * @return void
     */
    public function shouldGetData()
    {
        $products = New Product();
        $user = New User();
        $result = !empty($products->getAllProducts(null));
        $this->assertTrue($result);
    }

    /**
     * get single product.
     * @test
     * @return void
     */
    public function shouldGetsingleProduct()
    {
        $products = New Product();
        $isEmpty = empty($products->getSingleProducts(1));
        $this->assertFalse($isEmpty);
    }

    /**
     * Get init bid amount.
     * @test
     * @return void
     */
    public function shouldGetProductinitBid()
    {
        $product = New Product();
        $isEmpty = empty($product->getProductinitBid(1));
        $this->assertFalse($isEmpty);
    }

      /**
     * set bid end date.
     * @test
     * @return void
     */
    public function shouldSetBidEndDateTime()
    {
        $product = New Product();
        $result = $product->setBidEndDateTime(1, '2021-06-15 02:00:11');
        $result = $result === 1? true:false;
        $this->assertTrue($result);
    }

    
}
