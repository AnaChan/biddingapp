<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use Illuminate\Support\Facades\DB;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     * @test
     * @return void
     */
    public function validUser()
    {
        $user = New User();
        $result = DB::table('users')->select(['email','password'])->get();
        $this->assertTrue($user->isValidUser($result->first()->email, $result->first()->password));
    }

    /**
     * A basic feature test example.
     * @test
     * @return void
     */
    public function noValidUser()
    {
        $user = New User();
        $this->assertFalse($user->isValidUser('dsads', Str::random(7)));
    }
}
